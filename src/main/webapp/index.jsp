<%--
  Created by IntelliJ IDEA.
  User: indika
  Date: 3/31/2023
  Time: 5:12 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Welcome</title>
</head>
<body>
<h1>Welcome to User Management</h1>
<input type="button" value="Register User" onclick="registerPage();">
<input type="button" value="User Update" onclick="userUpdate();">
<input type="button" value="User Delete" onclick="userDelete();">
<input type="button" value="User Search" onclick="userSearch();">
</body>
</html>

<script>
    registerPage = () => {
        window.location.replace("user_register_done.jsp");
    }
    userSearch = () => {
        window.location.replace("<%=request.getContextPath() %>/AllUserServlet");
    }
    userUpdate = () => {
        window.location.replace("user_update.jsp");
    }
    userDelete = () => {
        window.location.replace("user_delete.jsp");
    }
</script>