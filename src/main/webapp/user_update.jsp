<%--
  Created by IntelliJ IDEA.
  User: indika
  Date: 3/31/2023
  Time: 9:01 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Update User</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script defer src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/js/bootstrap.bundle.min.js"></script>

</head>
<body>
<div class="container-fluid">
    <div class="row justify-content-center mt-5">
        <div class="col-6">
            <h1>Update User</h1>
            <form class="row g-3 mt-5 needs-validation" novalidate method="post"
                  action="<%=request.getContextPath()%>/userUpdateServlet">

                <div class="mb-3">
                    <label for="userid" class="form-label">Enter User Id</label>
                    <input type="text" class="form-control" id="userid" name="userid">
                </div>

                <div class="col-md-4">
                    <label for="firstname" class="form-label">First name</label>
                    <input type="text" class="form-control" id="firstname" name="firstname" required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="lastname" class="form-label">Last name</label>
                    <input type="text" class="form-control" id="lastname" name="lastname" required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="email" class="form-label">E-Mail</label>
                    <input type="email" class="form-control" id="email" name="email" required>
                </div>
                <div class="col-md-4">
                    <label for="nic" class="form-label">NIC</label>
                    <input type="text" class="form-control" id="nic" name="email" min="10">
                </div>

                <div class="col-md-4">
                    <label for="username" class="form-label">Username</label>
                    <div class="input-group has-validation">
                        <span class="input-group-text" id="inputGroupPrepend">@</span>
                        <input type="text" class="form-control" name="username" id="username"
                               aria-describedby="inputGroupPrepend"
                               required>
                        <div class="invalid-feedback">
                            Please choose a username.
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="password" class="form-label">Password</label>
                    <div class="input-group has-validation">
                        <input type="text" class="form-control" name="password" id="password" required>
                    </div>
                </div>

                <div class="col-12">
                    <button class="btn btn-primary" type="submit">Update User</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
