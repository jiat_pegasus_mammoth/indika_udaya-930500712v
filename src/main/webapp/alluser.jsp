<%@ page import="java.sql.ResultSet" %><%--
  Created by IntelliJ IDEA.
  User: indika
  Date: 3/31/2023
  Time: 8:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>All User</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script defer src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
<div class="container-fluid">
    <div class="row justify-content-center mt-5">
        <div class="col-6">
            <h1>View All User</h1>
        </div>
        <div class="col-6 mt-5">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">ID</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">NIC</th>
                    <th scope="col">User Name</th>
                </tr>
                </thead>
                <tbody>
                <%
                    ResultSet rs = (ResultSet) request.getAttribute("DataDB");
                    while (rs.next()) {
                %>
                <tr>
                    <th scope="row">1</th>
                    <td>rs.getString(1)</td>
                    <td>rs.getString(2)</td>
                    <td>rs.getString(3)</td>
                    <td>rs.getString(4)</td>
                    <td>rs.getString(5)</td>
                    <td>rs.getString(6)</td>
                    <td>rs.getString(7)</td>
                </tr>
                <%
                    }
                %>

                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
