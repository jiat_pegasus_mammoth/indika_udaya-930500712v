<%--
  Created by IntelliJ IDEA.
  User: indika
  Date: 3/31/2023
  Time: 9:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Delete User</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script defer src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/js/bootstrap.bundle.min.js"></script>

</head>
<body>

<div class="container-fluid">
    <div class="row justify-content-center mt-5">
        <div class="col-6">
            <h1>Delete User</h1>
            <form class="row g-3 needs-validation" novalidate method="post"
                  action="<%=request.getContextPath()%>/deleteUserServlet">

                <div class="mb-3">
                    <label for="userid" class="form-label">Enter User Id</label>
                    <input type="text" class="form-control" id="userid" name="userid">
                </div>
                <div class="col-12">
                    <button class="btn btn-primary" type="submit">Delete User</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
