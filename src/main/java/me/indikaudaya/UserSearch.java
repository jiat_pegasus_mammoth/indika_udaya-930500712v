package me.indikaudaya;

import me.indikaudaya.db.DBConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserSearch {
    ResultSet resultSet;

    public ResultSet searchUser(int userId) {

        try (Connection connection = DBConnection.getConnection()) {

            String search;

            if (userId == 0) {
                search = "SELECT * FROM `user`";
            } else {
                search = "SELECT * FROM `user` WHERE `id`=? ";
            }

            PreparedStatement preparedStatement = connection.prepareStatement(search);
            if (userId != 0) {
                preparedStatement.setInt(1, userId);
            }

            resultSet = preparedStatement.executeQuery();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultSet;

    }
}
