package me.indikaudaya.controller;

import me.indikaudaya.UserUpdate;
import me.indikaudaya.model.User;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "UserUpdateServlet", value = "/userUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("alluser.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("userid");
        String firstName = request.getParameter("firstname");
        String lastName = request.getParameter("lastname");
        String email = request.getParameter("email");
        String nic = request.getParameter("nic");
        String userName = request.getParameter("username");
        String password = request.getParameter("password");

        String s = new UserUpdate().updateUser(Integer.parseInt(id), new User(firstName, lastName, email, nic, userName, password));
        response.getWriter().write("<h2>" + s + " </h2>");
        request.getRequestDispatcher("user_update.jsp").include(request, response);
    }
}
