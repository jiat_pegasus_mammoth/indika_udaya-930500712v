package me.indikaudaya.controller;

import me.indikaudaya.UserData;
import me.indikaudaya.model.User;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "UserRegister", value = "/userregister")
public class UserServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // response.getWriter().println("adada");
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("user_register_done.jsp");
        requestDispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String firstName = request.getParameter("firstname");
        String lastName = request.getParameter("lastname");
        String email = request.getParameter("email");
        String nic = request.getParameter("nic");
        String userName = request.getParameter("username");
        String password = request.getParameter("password");

        new UserData().registerUser(new User(firstName, lastName, email, nic, userName, password));

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("user_register.jsp");
        requestDispatcher.forward(request, response);
    }
}
