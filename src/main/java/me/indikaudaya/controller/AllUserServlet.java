package me.indikaudaya.controller;

import me.indikaudaya.UserSearch;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.ResultSet;

@WebServlet(name = "AllUserServlet", value = "/AllUserServlet")
public class AllUserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ResultSet resultSet = new UserSearch().searchUser(0);
        request.setAttribute("DataDB",resultSet);
        request.getRequestDispatcher("alluser.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
