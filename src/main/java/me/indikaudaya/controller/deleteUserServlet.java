package me.indikaudaya.controller;

import me.indikaudaya.UserDelete;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "deleteUserServlet", value = "/deleteUserServlet")
public class deleteUserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("user_delete.jsp").forward(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("userid");
        String status = new UserDelete().DeleteUser(Integer.parseInt(id));
        response.getWriter().write("<h2> User delete successfully....</h2>");
        request.getRequestDispatcher("user_delete.jsp").include(request, response);
    }
}
