package me.indikaudaya.db;

import me.indikaudaya.util.ApplicationProperties;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

    private static Connection connection;

    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        if (connection == null) {
            ApplicationProperties applicationProperties = ApplicationProperties.getInstance();
            Class.forName(applicationProperties.getValue("sql.connection.driver"));
            connection = DriverManager.getConnection(
                    applicationProperties.getValue("sql.connection.url"),
                    applicationProperties.getValue("sql.connection.user"),
                    applicationProperties.getValue("sql.connection.password")
            );
        }
        return connection;
    }
}
