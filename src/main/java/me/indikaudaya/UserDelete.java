package me.indikaudaya;

import me.indikaudaya.db.DBConnection;
import me.indikaudaya.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserDelete {
    public String DeleteUser(int userId) {
        String status = "";
        try (Connection connection = DBConnection.getConnection()) {

            ResultSet resultSet = new UserSearch().searchUser(userId);
            if (resultSet.first()) {

                String deleteQ = "DELETE FROM `user` WHERE `id`=?";

                PreparedStatement preparedStatement = connection.prepareStatement(deleteQ);
                preparedStatement.setInt(1, userId);

                int i = preparedStatement.executeUpdate();
                status = "Delete successfully.." + i;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }
}
