package me.indikaudaya;

import me.indikaudaya.db.DBConnection;
import me.indikaudaya.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserUpdate {

    public String updateUser(int userId, User user) {
        String status = "";
        try (Connection connection = DBConnection.getConnection()) {

            ResultSet resultSet = new UserSearch().searchUser(userId);
            if (resultSet.first()) {

                String updateQ = "UPDATE `user` SET `firstname`=?,`lastname`=?,`email`=?,`nic`=?,`username`=?,`password`=? WHERE `id`=?";

                PreparedStatement preparedStatement = connection.prepareStatement(updateQ);
                preparedStatement.setString(1, (user.getFirstName() == null ? resultSet.getString(2) : user.getFirstName()));
                preparedStatement.setString(2, (user.getLastName() == null ? resultSet.getString(3) : user.getLastName()));
                preparedStatement.setString(3, (user.getEmail() == null ? resultSet.getString(4) : user.getEmail()));
                preparedStatement.setString(4, (user.getNic() == null ? resultSet.getString(5) : user.getNic()));
                preparedStatement.setString(5, (user.getUserName() == null ? resultSet.getString(6) : user.getUserName()));
                preparedStatement.setString(6, (user.getPassword() == null ? resultSet.getString(7) : user.getPassword()));
                preparedStatement.setInt(7, userId);

                int i = preparedStatement.executeUpdate();
                status = "Update successfully.." + i;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }
}
