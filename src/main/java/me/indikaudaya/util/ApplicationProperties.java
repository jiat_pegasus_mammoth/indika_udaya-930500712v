package me.indikaudaya.util;

import java.io.IOException;
import java.util.Properties;

public class ApplicationProperties {
    private static ApplicationProperties applicationProperties;
    private Properties properties;

    private ApplicationProperties() {
        properties = new Properties();
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream("application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ApplicationProperties getInstance() {
        if (applicationProperties == null)
            applicationProperties = new ApplicationProperties();
        return applicationProperties;
    }

    public String getValue(String key) {
        return properties.getProperty(key);
    }

}
