package me.indikaudaya;

import me.indikaudaya.db.DBConnection;
import me.indikaudaya.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class UserData {
    public void registerUser(User user) {
        String saveQuery = "INSERT INTO `user`" +
                "(`firstname`,`lastname`,`email`,`nic`,`username`,`password`) VALUES " +
                "(?,?,?,?,?,?) ";

        try (Connection connection = DBConnection.getConnection()) {

            PreparedStatement preparedStatement = connection.prepareStatement(saveQuery);
            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getNic());
            preparedStatement.setString(5, user.getUserName());
            preparedStatement.setString(6, user.getPassword());
            preparedStatement.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
